/**
 * Analyzer Service
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict'

const analyzer = {}

/**
 * Get avarage of given numbers
 * @param values
 * @returns {number}
 */
analyzer.getAverage = function (values) {
  return values.reduce(function(a, b){return parseInt(a)+parseInt(b)}) / values.length
}

module.exports = analyzer