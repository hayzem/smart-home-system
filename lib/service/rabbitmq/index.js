/**
 * RabbitMQ Service
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict';

/**
 * Get config and logger
 */
const config = require('../../../config')
const logger = require('winston')

const rabbitmq = {}

rabbitmq.ch = null
rabbitmq.connection = require('amqplib').connect(config.rabbitmq.uri)

/**
 * create rabbitmq channel
 */
rabbitmq.getConnection = function () {
  let self = this

  return rabbitmq.connection.then(function(conn) {
    if(null == self.ch){
      self.ch = conn.createChannel()
      console.log("channel has been created.")
    }

    return self.ch
  }).catch(function (e) {
    console.log(e.message)
    logger.log('error', e.message)
  })
}

/**
 * @param queue
 * @param exchange
 * @param msg
 * @param options
 */
rabbitmq.publish = function (queue, exchange, msg, options) {
  rabbitmq.getConnection().then(function(ch) {
    let ex = ch.assertExchange(exchange, 'fanout', {durable: false})

    return ex.then(function() {
      return ch.assertQueue(queue).then(function(ok) {
        ch.publish(exchange, queue, new Buffer(JSON.stringify(msg)))
      })
    })
  }).catch(console.warn);
}

/**
 * @param queue
 * @param exchange
 * @param key
 * @param ack
 * @param callback
 */
rabbitmq.consume = function (queue, exchange, key, ack, callback) {
  let self = this
  return self.getConnection().then(function (ch) {
    let ok = ch.assertQueue(queue, {exclusive: true});

    if(exchange){
      ok = ch.assertExchange(exchange, 'fanout', {durable: false})
        .then(function() {
          return ch.bindQueue(queue, exchange, key)
        });
    }

    ok = ok.then(function() {
      return ch.consume(queue, returnMessage, {noAck: true});
    });

    return ok.then(function() {
      console.log(' [*] Waiting for messages from "' + queue + '"...');
    });

    function returnMessage(message) {
      if (message !== null) {
        if(ack) ch.ack(message);
        callback({
          content: message.content.toString(),
          routingKey: message.fields.routingKey,
        })
      }
    }
  });
}

module.exports = rabbitmq