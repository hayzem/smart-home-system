/**
 * Database Connection
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict';

const config = require('../../config/index')
const logger = require('winston')

const database = {}

database.mongoose = require('mongoose')
database.mongoose.Promise = require('promise');

/**
 * Connect to database, get connection
 */
database.connect = function () {
  database.mongoose.connect(config.mongodb.uri + config.mongodb.db.name);
  database.mongoose.connection.on('error', function(){
    logger.log('error','MongoDB Connection Error. Please make sure that MongoDB is running.')
    process.exit(1)
  })
}

database.connect()

module.exports = database