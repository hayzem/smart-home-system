/**
 * Room Repository
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict'

/**
 * Get db connection and model
 */
const database = require('../../database')
const RoomModel = require('../../../models/room').model;

/**
 * Get one room
 * @param callback
 * @returns {Promise}
 */
database.getRoom = function (callback) {
  let promise = RoomModel.findOne().exec()
  if(callback) {
    callback(promise)
  }

  return promise
}

/**
 * Get one room by id
 * @param id
 * @param callback
 * @returns {Promise}
 */
database.getRoomById = function (id, callback) {
  let promise = RoomModel.findOne({_id: id}).exec()
  if(callback) {
    callback(promise)
  }

  return promise
}

/**
 * Get list of rooms
 * @param callback
 * @returns {Promise|*|RegExpExecArray}
 */
database.getRooms = function (callback) {
  let promise = RoomModel.find().exec()
  if(callback) {
    callback(promise)
  }

  return promise
}

module.exports = database