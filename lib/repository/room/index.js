/**
 * Sensor Repository
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict'

/**
 * Get room repository
 * @type {*|database}
 */
const database = require('./repository')

module.exports = database