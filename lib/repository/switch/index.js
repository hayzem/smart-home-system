/**
 * Switch Repository
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict'

/**
 * Get switch repository
 * @type {*|database}
 */
const database = require('./repository')

module.exports = database