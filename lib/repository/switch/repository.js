const database = require('../../database')

const SwitchModel = require('../../../models/switch').model;

/**
 * Get first switch matches with room.id
 * @param roomId
 * @param callback
 * @returns {Promise}
 */
database.getSwitchByRoomId = function (roomId, callback) {
  let promise = SwitchModel.findOne({'roomId': roomId}).exec()
  if(callback) {
    callback(promise)
  }

  return promise
}

/**
 * Get all switches
 * @param callback
 * @returns {Promise|*|RegExpExecArray}
 */
database.getSwitches = function (callback) {
  let promise = SwitchModel.find().exec()
  if(callback) {
    callback(promise)
  }

  return promise
}

module.exports = database