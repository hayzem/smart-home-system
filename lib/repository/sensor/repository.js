/**
 * Sensor Repository
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict'

/**
 * Get db connection and model
 */
const database = require('../../database')
const SensorModel = require('../../../models/sensor').model;

/**
 * Get sensor by id
 * @param id
 * @param callback
 * @returns {Promise}
 */
database.getSensorById = function (id, callback) {
  let promise = SensorModel.findById(id).exec()
  if(callback) {
    callback(promise)
  }

  return promise
}

/**
 * Get list of sensors
 * @param query
 * @param callback
 * @returns {Promise|*|RegExpExecArray}
 */
database.getSensors = function (query, callback) {
  let promise = SensorModel.find(query).exec()
  if(callback) {
    callback(promise)
  }

  return promise
}

/**
 * Get sensor by room.id
 * @param roomId
 * @param callback
 * @returns {Promise|*|RegExpExecArray}
 */
database.getSensorsByRoomId = function (roomId, callback) {
  let promise = SensorModel.find({'roomId': roomId}).exec()
  if(callback) {
    callback(promise)
  }

  return promise
}

module.exports = database