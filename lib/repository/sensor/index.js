/**
 * Sensor Repositories
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict'

/**
 * Get sensor repository
 * @type {*|database}
 */
const database = require('./repository')

module.exports = database