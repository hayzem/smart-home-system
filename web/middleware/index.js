/**
 * Middleware(s) for serving content
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict'

/**
 * Get middleware(s)
 */
const favicon = require('serve-favicon');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const sassMiddleware = require('node-sass-middleware');
const compression = require('compression');
const winMid = require('express-winston-middleware');

module.exports = { compression, bodyParser, cookieParser, sassMiddleware, favicon, winMid};
