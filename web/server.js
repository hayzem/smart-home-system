/**
 * Server
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict'

/**
 * Get dependencies
 */
const middleware = require('./middleware')
const winston = require('winston');
const path = require('path');
const express = require('express');
const app = express();

/**
 * Set view path and view engine
 */
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

/**
 * @todo: add middlewares dynamically
 */
app.use(middleware.favicon(path.join(__dirname, '../public', 'favicon.ico')));
app.use(middleware.compression());
app.use(middleware.bodyParser.json());
app.use(middleware.bodyParser.urlencoded({extended: false}));
app.use(middleware.cookieParser());
app.use(middleware.sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: true, // true = .sass and false = .scss
  sourceMap: true
}));

//@todo: set environment base logging
app.use(new middleware.winMid.request({
  transports: [
    new (winston.transports.Console)({ json: true })
  ]
}));

/**
 * Set routes
 */
app.use(require('./controller'))

/**
 * Set path to serve static files
 */
app.use(express.static(path.join(__dirname, '../public')));

/**
 * catch 404 and forward to error handler
 */
app.use(function (req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

/**
 * error handler
 */
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;