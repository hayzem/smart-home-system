/**
 * Web Main
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict'

/**
 * Module dependencies.
 */
const app = require('./server');
const http = require('http');
const logger = require('winston')
const config = require('../config')

/**
 * Set port
 */
app.set('port', config.server.port);

/**
 * Create HTTP server.
 */
const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(config.server.port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Event listener for HTTP server "error" event.
 */
function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  let bind = typeof config.server.port === 'string'
    ? 'Pipe ' + config.server.port
    : 'Port ' + config.server.port;

  /**
   * handle specific listen errors with friendly messages
   */
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
  let addr = server.address();
  let bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + config.server.port;
  logger.log('info','Listening on ' + bind);
}
