/**
 * Main Controller
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict'

/**
 * Get dependencies
 * @type {*|createApplication}
 */
const express = require('express')
  ,router = express.Router()

/**
 * Home page
 */
router.get('/', function (req, res, next) {
  res.render('index', {title: 'Smart Home System'});
});

/**
 * Sensor routes
 */
router.use('/sensors', require('./sensors'));

/**
 * Room routes
 */
router.use('/rooms', require('./rooms'));

module.exports = router