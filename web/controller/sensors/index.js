/**
 * Sensor Controller
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict'

/**
 * Get dependencies
 * @type {*|createApplication}
 */
const express = require('express')
const router = express.Router()
const sensorDb = require('../../../lib/repository/sensor')

/* GET list */
router.get('/', function (req, res, next) {
  sensorDb.getSensors()
    .then(function (sensors) {
      res.render('sensors/index', {title: 'Smart Home System - Sensor List', sensors: sensors})
    }).catch(function (e) {
      res.render('error', {message: e.message, error: e})
    })
})

/* GET detail */
router.get('/:id', function (req, res, next) {
  sensorDb.getSensorById(req.params.id).then(function (sensor) {
    /**
     * Set websocket connection paths
     * @todo: moves these variables to config
     */
    const host = req.headers.host.replace(/:.*/, '')
    const wsLibraryUrl = 'http://' + host + ':' + process.env.SOCKET_IO_PORT + '/socket.io/socket.io.js'
    const wsServerUrl = 'ws://' + host + ':' + process.env.SOCKET_IO_PORT

    sensor.data = sensor.data.slice(0, 20)

    res.render('sensors/show', {
      title: 'Smart Home System - ' + sensor.name,
      sensor: sensor,
      wsLibraryUrl: wsLibraryUrl,
      wsServerUrl: wsServerUrl
    })
  }).catch(function (e) {
    res.render('error', {message: e.message, error: e})
  })
})

module.exports = router
