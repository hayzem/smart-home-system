/**
 * Room Controller
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict'

/**
 * Get dependencies
 * @type {*|createApplication}
 */
const express = require('express')
const router = express.Router()
const roomDb = require('../../../lib/repository/room')
const sensorDb = require('../../../lib/repository/sensor')

/**
 * Get and serve list of rooms
 */
router.get('/', function (req, res, next) {
  roomDb.getRooms()
    .then(function (rooms) {
      res.render('rooms/index', {title: 'List of Rooms', rooms: rooms})
    }).catch(function (e) {
      throw new Error(e)
    })
})

/**
 * Get and serve details of a room
 */
router.get('/:id', function (req, res, next) {
  roomDb.getRoomById(req.params.id)
    .then(function (room) {
      /**
       * Set websocket connection paths
       * @todo: moves these variables to config
       */
      const host = req.headers.host.replace(/:.*/, '')
      const wsLibraryUrl = 'http://' + host + ':' + process.env.SOCKET_IO_PORT + '/socket.io/socket.io.js'
      const wsServerUrl = 'ws://' + host + ':' + process.env.SOCKET_IO_PORT

      sensorDb.getSensorsByRoomId(room._id.toString())
        .then(function (sensors) {
          for (let i=0; i < sensors.length; i++) {
            sensors[i].data  = sensors[i].data.slice(sensors[i].data.length-20, sensors[i].data.length)
          }

          res.render('rooms/show', {
            title: 'Smart Home System - ' + room.name,
            room: room,
            sensors: sensors,
            wsLibraryUrl: wsLibraryUrl,
            wsServerUrl: wsServerUrl
          })
        })
        .catch(function (e) {
          throw new Error(e.message)
        })

    }, function (err) {
      res.render('error', {message: err.message, error: err})
    })
})

module.exports = router
