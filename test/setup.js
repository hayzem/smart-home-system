/**
 * Setup Test Environment
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict'

/**
 * Get config and dependencies
 */
const config = require('../config')
const fixtures = require('pow-mongodb-fixtures').connect(config.mongodb.db.name)

/**
 * Get test data fixtures
 */
const roomFixtures = require('./fixtures/rooms')
const sensorFixtures = require('./fixtures/sensors')
const switchFixtures = require('./fixtures/switches')

/**
 * Load test data fixtures
 */
fixtures.load(roomFixtures, function () {
  console.log('# room(s) loaded to database')
  let roomId = roomFixtures.room[0]._id.toString()
  for (let i in sensorFixtures.sensor) {
    sensorFixtures.sensor[i].roomId = roomId
  }
  fixtures.load(sensorFixtures, function () {
    console.log('# sensors(s) loaded database')
  })
  for (let i in switchFixtures.switch) {
    switchFixtures.switch[i].roomId = roomId
  }
  fixtures.load(switchFixtures, function () {
    console.log('# switches(s) loaded database')
  })
})