/**
 * Sensor Test Data
 * @author Ali Atasever <aliatasever@gmail.com>
 */

/**
 * Set test sensor data
 * @type {*[]}
 */
exports.sensor = [
    { name: 'Temperature Receptor T01', type: 'temperature', unit: 'C', roomId: '', data: []},
    { name: 'Temperature Receptor T02', type: 'temperature', unit: 'C', roomId: '', data: []},
    { name: 'Temperature Receptor T03', type: 'temperature', unit: 'C', roomId: '', data: []},
    { name: 'Temperature Receptor T04', type: 'temperature', unit: 'C', roomId: '', data: []},
    { name: 'Temperature Receptor T05', type: 'temperature', unit: 'C', roomId: '', data: []},
    { name: 'Temperature Receptor T06', type: 'temperature', unit: 'C', roomId: '', data: []},
    { name: 'Temperature Receptor T07', type: 'temperature', unit: 'C', roomId: '', data: []},
];