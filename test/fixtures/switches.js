/**
 * Switch Test Data
 * @author Ali Atasever <aliatasever@gmail.com>
 */

/**
 * Set test switch data
 * @type {*[]}
 */
exports.switch = [
  { name: 'Heater Switch', type:'two-state-heater', roomId: ''},
  { name: 'Air Condition Switch', type:'two-state-ac', roomId: ''}
];