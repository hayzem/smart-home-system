/**
 * Room Test Data
 * @author Ali Atasever <aliatasever@gmail.com>
 */

/**
 * Get dependencies
 */
let id = require('pow-mongodb-fixtures').createObjectId;

/**
 * Set test room data
 * @type {*[]}
 */
exports.room = [
  { _id: id(), name: 'Room A', idealTemperature: '22', idealTemperatureUnit: 'C'},
  { _id: id(), name: 'Room B', idealTemperature: '16', idealTemperatureUnit: 'C'}
];