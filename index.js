'use strict'

const logger = require('winston')
const type = process.env.PROCESS_TYPE

logger.info(`Starting '${type}' process`, { pid: process.pid })

if (type === 'web') {
  require('./web')
} else if (type === 'mock-sensors') {
  require('./scripts/mock/sensors')
} else if (type === 'websocket-server') {
  require('./worker/websocket')
} else {
  throw new Error(`
    ${type} is an unsupported process type. 
    Use one of: 'web', 'twitter-stream-worker', 'social-preprocessor-worker'!
  `)
}


