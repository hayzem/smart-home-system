/**
 * Universal Config
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict';

/**
 * Set .env definitions to environment variables
 */
require('dotenv').config();

/**
 * Include configs
 */
const common = require('./components/common');
const logger = require('./components/logger');
const rabbitmq = require('./components/rabbitmq');
const server = require('./components/server');
const mongodb = require('./components/mongodb');
const analyzer = require('./components/analyzer');

module.exports = Object.assign({}, common, logger, server, rabbitmq, mongodb, analyzer);