/**
 * Analyzer Config
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict'

/**
 * Get validator
 */
const joi = require('joi')

const envVarsSchema = joi.object({
  ANALYZER_CLOCK: joi.string().required(),
}).unknown().required()

const { error, value: envVars } = joi.validate(process.env, envVarsSchema)
if (error) {
  throw new Error(`Config validation error: ${error.message}`)
}

const config = {
  analyzer: {
    clock: envVars.ANALYZER_CLOCK,
  }
}

module.exports = config
