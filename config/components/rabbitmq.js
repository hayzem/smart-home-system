/**
 * RabbitMq Config
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict'

/**
 * Get validator
 */
const joi = require('joi')

const envVarsSchema = joi.object({
  RABBITMQ_URI: joi.string().required(),
}).unknown().required()

const { error, value: envVars } = joi.validate(process.env, envVarsSchema)
if (error) {
  throw new Error(`Config validation error: ${error.message}`)
}

const config = {
  rabbitmq: {
    uri: envVars.RABBITMQ_URI,
  }
}

module.exports = config
