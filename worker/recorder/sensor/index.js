/**
 * Record Sensor Data
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict'

module.exports = require('./recorder')
