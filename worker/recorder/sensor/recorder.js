/**
 * Sensor Data Recorder
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict'

/**
 * Get dependencies
 */
const logger = require('winston')
const rabbitmqService = require('../../../lib/service/rabbitmq/index')
const sensorDb = require('../../../lib/repository/sensor')
const Sensor = require('../../../models/sensor')

/**
 * Configure logger
 */
logger.configure({
  transports: [
    new (logger.transports.File)({filename: 'var/logs/recorder.log'})
  ]
})

/**
 * Get sensor data and save
 */
sensorDb.getSensors().then(function (sensors) {
  rabbitmqService.consume('recorder_sensor_data', 'sensors', '*', false, function (message) {
    let sensor = sensors.find(function (a) { return a._id.toString() === message.routingKey})
    const value = new Sensor.modelValue({
      value: JSON.parse(message.content).temperature
    })

    sensor.data.push(value)
    sensor.save(function (err) {
      if(err) console.log(err)
    })
  })
  .catch(function (e) {
    console.error(e.message)
  })
})

