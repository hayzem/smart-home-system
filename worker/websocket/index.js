/**
 * Websocket Server
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict'

module.exports = require('./server')
