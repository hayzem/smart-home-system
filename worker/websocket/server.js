/**
 * Websocket Server
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict'

/**
 * Get dependencies
 */
const io = require('socket.io')()
const logger = require('winston')
const rabbitmqService = require('../../lib/service/rabbitmq')

/**
 * Set logger
 */
logger.configure({
  transports: [
    new (logger.transports.File)({filename: 'var/logs/websocket.log'})
  ]
})

/**
 * Websocket when client connected
 */
io.on('connection', function (socket) {
  logger.log('info', 'client connected')

  /**
   * Get messages from rabbitmq queues by queue names sent from client
   */
  socket.on('getSensorUpdates', function (queueNames) {
    rabbitmqService.consume('websocket_' + hashQueueNames(queueNames), 'sensors', queueNames.join('.'), false, function (message) {
      let data = JSON.parse(message.content)
      console.log(message.routingKey + ' > ' +JSON.stringify(data))
      socket.emit('update', {
        sensorId: message.routingKey,
        data: data
      })
    })
  })
})

/**
 * Start websocket server
 */
io.listen(process.env.SOCKET_IO_PORT)

/**
 * Hash queue names to avoid multiple queues on RabbitMQ
 * @param queueNames
 * @returns {*|PromiseLike<ArrayBuffer>}
 */
function hashQueueNames (queueNames) {
  return require('crypto').createHash('md5').update(queueNames.join('.')).digest("hex")
}
