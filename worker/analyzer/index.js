/**
 * Analyze Sensor Data
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict'

/**
 * Get dependencies
 */
const config = require('../../config')
const logger = require('winston')
const analyzerService = require('../../lib/service/analyzer')
const rabbitmqService = require('../../lib/service/rabbitmq/index')
const roomDb = require('../../lib/repository/room')
const sensorDb = require('../../lib/repository/sensor')
const switchDb = require('../../lib/repository/switch')

/**
 * Set logger log file
 */
logger.configure({
  transports: [
    new (logger.transports.File)({filename: 'var/logs/websocket.log'})
  ]
})

/**
 * Get time to check sensor data to analyze from config
 */
let clock = config.analyzer.clock

roomDb.getRoom().then(function (room) {
  console.log('Analyzing Sensor Data for ' + room.name)
  setInterval(function () {
    sensorDb.getSensorsByRoomId(room._id.toString())
      .then(function (sensors) {
        let latestTemperatures = []
        sensors.forEach(function (sensor) {
          latestTemperatures.push(sensor.data[sensor.data.length - 1].value)
        })

        let average = analyzerService.getAverage(latestTemperatures)
        console.log('Average temperature is ' + average + ' ' + room.idealTemperatureUnit)

        if(average < room.idealTemperature){
          console.log(room.name + ' temperature is lower than ideal!')
          sendSwitchOnEvent(room)
        }else if(average > room.idealTemperature){
          console.log(room.name + ' temperature is higher than ideal!')
          sendSwitchOffEvent(room)
        }
      })
      .catch(function (e) {
        console.log(e.message)
      })
  }, clock)
}).catch(function (e) {
  console.log(e.message)
})

/**
 * Send switch event "ON" to mq
 * @param room
 */
function sendSwitchOnEvent (room) {
  switchDb.getSwitchByRoomId(room._id.toString())
    .then(function (roomSwitch) {
      console.log(roomSwitch.name + ' switching on.')
      rabbitmqService.publish(roomSwitch._id.toString(), 'switches', {
        roomId: room._id.toString(),
        apply: 1
      })
    })
}

/**
 * Send switch event "OFF" to mq
 * @param room
 */
function sendSwitchOffEvent (room) {
  switchDb.getSwitchByRoomId(room._id.toString())
    .then(function (roomSwitch) {
      console.log(roomSwitch.name + ' switching off.')
      rabbitmqService.publish(roomSwitch._id.toString(), 'switches', {
        roomId: room._id.toString(),
        apply: -1
      })
    })
}

