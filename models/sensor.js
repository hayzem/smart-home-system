/**
 * Sensor Model
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict'

/**
 * Get dependencies
 */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const sensorValueSchema = new Schema({
  value: String,
  date: {type: Date, default: Date.now}
})

/**
 * data can be problematic when big data is pushed
 * can be moved to another table and linked to here
 * we can leave it like this since this will not be in production
 * @type {mongoose.Schema}
 */
const sensorSchema = new Schema({
    name: { type: String },
    uid: { type: String },
    type: { type: String },
    unit: { type: String },
    roomId: { type: String },
    data: [ sensorValueSchema ]
}, { collection: 'sensor' });

module.exports = {
    schema: sensorSchema,
    valueSchema: sensorValueSchema,
    model: mongoose.model('Sensor', sensorSchema),
    modelValue: mongoose.model('SensorValue', sensorValueSchema)
};