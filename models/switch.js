/**
 * Switch Model
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict'

/**
 * Get dependencies
 */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const switchSchema = new Schema({
  name: { type: String },
  type: { type: String },
  state: { type: Number, default: 0 },
  roomId: { type: String }
}, { collection: 'switch' });

module.exports = {
  schema: switchSchema,
  model: mongoose.model('Switch', switchSchema)
};