/**
 * Room Model
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const roomSchema = new Schema({
  name: { type: String },
  idealTemperature: {type: String},
  idealTemperatureUnit: {type: String},
}, { collection: 'room' });

module.exports = {
  schema: roomSchema,
  model: mongoose.model('Room', roomSchema)
};