/**
 * Websocket Client for Sensor Updates
 * @author Ali Atasever <aliatasever@gmail.com>
 */

/**
 * Update Temperature Graph
 * @param updateData
 * @param chart
 * @param sensor
 */
function updateTemperatureGraph (updateData, chart, sensor) {
  if(updateData.sensorId === sensor._id.toString()){
    chart.data.datasets.forEach(function(dataset){
      if(dataset.label === sensor.name){
        chart.data.labels.push(moment(updateData.data.date).format('h:mm:ss.SSS a'));
        dataset.data.push(updateData.data.temperature);
      }
    });
    chart.update();
  }
}

/**
 * Get char label by sensor.id
 * @param id
 * @param sensorLabels
 * @returns {string}
 */
function getLabelById(id, sensorLabels){
  var label = '';
  sensorLabels.forEach(function (sensorLabel) {
    if(sensorLabel.id === id){
      label = sensorLabel.label;
    }
  })

  return label
}

$(function () {
  /**
   * Connect to websocket server
   */
  var socket = io(wsServerUrl);

  /**
   * Send sensor id(s) to get updates on "connect" event
   */
  socket.on('connect', function(){
      console.log('connected!');
      socket.emit('getSensorUpdates', [sensor._id]);
  });

  /**
   * Update data and chart on "update" event
   */
  socket.on('update', function(updateData){
      updateTemperatureGraph(updateData, sensorChart, sensor);
  });
});