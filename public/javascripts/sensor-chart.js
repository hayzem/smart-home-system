/**
 * Sensor Chart
 * "sensor" should be defined in the scope
 * @author Ali Atasever <aliatasever@gmail.com>
 */

var sensorChartLabels = []
var sensorChartDataset = []

sensor.data.forEach(function (sensorData) {
  sensorChartLabels.push(moment(sensorData.date).format('h:mm:ss.SSS a'));
  sensorChartDataset.push(sensorData.value);
});

var ctx = document.getElementById('sensor_chart').getContext('2d');
var sensorChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: sensorChartLabels,
    datasets: [
      {
        label: sensor.name,
        data: sensorChartDataset,
      }
    ],
  },
  options: {
    xAxes: [{
      type: 'time',
      time: {
        parser: 'h:mm:ss.SSS a',
      },
      distribution: 'series'
    }]
  }
});