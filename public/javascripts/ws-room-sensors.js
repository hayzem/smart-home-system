/**
 * Websocket Client for Sensor Updates
 * @author Ali Atasever <aliatasever@gmail.com>
 */

/**
 * Update Temperature Graph
 * @param updateData
 * @param chart
 * @param sensorLabels
 */
function updateTemperatureGraph (updateData, chart, sensorLabels) {
    var data = updateData.data;
    var label = getLabelById(updateData.sensorId, sensorLabels);
    chart.data.datasets.forEach((dataset) => {
        if(dataset.label === label){
          var date = moment(data.date)
            dataset.data.push({
                x: date.format('h:mm:ss.SSS a'),
                y: data.temperature,
            });
        }
    });
    chart.update();
}

/**
 * Get char label by sensor.id
 * @param id
 * @param sensorLabels
 * @returns {string}
 */
function getLabelById(id, sensorLabels){
    var label = '';
    sensorLabels.forEach(function (sensorLabel) {
        if(sensorLabel.id === id){
            label = sensorLabel.label;
        }
    })

    return label
}

$(function () {
  var sensorIds = [];
  var sensorLabels = []
  sensors.forEach(function (sensor) {
      sensorIds.push(sensor._id.toString())
      sensorLabels.push({
          id: sensor._id.toString(),
          label: sensor.name,
      });
  });

  /**
   * Connect to websocket
   */
  var socket = io(wsServerUrl);

  /**
   * Send list of sensor id(s) to get updates on connect event
   */
  socket.on('connect', function(){
      console.log('connected!');
      socket.emit('getSensorUpdates', sensorIds);
  });

  /**
   * Update chart on "update" event
   */
  socket.on('update', function(updateData){
        updateTemperatureGraph(updateData, roomSensorsChart, sensorLabels);
    });
});