/**
 * Multiple Sensors Chart
 * "sensors" should be defined in the scope
 * @author Ali Atasever <aliatasever@gmail.com>
 */

/**
 * Get Chart.js chart
 * @param sensors
 * @param ctx
 */
function getChart (sensors, ctx) {
  var datasets = []
  sensors.forEach(function(sensor){
    var sensorChartData = [];
    sensor.data.forEach(function (sensorData) {
      var date = moment(sensorData.date);
      sensorChartData.push({
        x: date.format('h:mm:ss.SSS a'),
        y: sensorData.value,
      })
    });

    sensorChartData.sort(function (a, b) {
      var dateA = moment(a.x, 'h:mm:ss.SSS a');
      var dateB = moment(b.x, 'h:mm:ss.SSS a');
      return dateA.isBefore(dateB) ? -1 : dateA.isAfter(dateB) ? 1 : 0;
    })

    datasets.push({
      label: sensor.name,
      data: sensorChartData,
      fill: false,
      borderColor: getRandomColor(),
    })
  });

  return new Chart(ctx, {
    type: 'line',
    data: {
      datasets: datasets,
    },
    options: {
      scales: {
        xAxes: [{
          type: 'time',
          time: {
            parser: 'h:mm:ss.SSS a',
          },
          distribution: 'series'
        }]
      }
    }
  });
}

/**
 * Get random color code
 * @returns {string}
 */
function getRandomColor() {
  var letters = '0123456789ABCDEF'.split('');
  var color = '#';
  for (var i = 0; i < 6; i++ ) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

/**
 * Set DOM element and get chart
 * @type {(CanvasRenderingContext2D | null) | (WebGLRenderingContext | null) | (CanvasRenderingContext2D | WebGLRenderingContext | null)}
 */
var chartCanvas = document.getElementById('room_sensors_chart').getContext('2d');

/**
 * Set chart
 */
var roomSensorsChart = getChart(sensors, chartCanvas);