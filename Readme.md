
# Smart Home System

This project is a test case of how smart home system devices can collect data, analyze and take actions. 


[![Express Logo](https://i.hizliresim.com/LblajZ.png)](https://bitbucket.org/hayzem/smart-home-system)

Get real time sensors data in specific room. Currently in this project only heat sensors are defined. If the room temperature is not ideal switches are activated. This means that if the switch is a heater or AC, they will be turned ON or OFF.

## Requirements
* MongoDB
* RabbitMQ
* Node.js

## Installation

Before installing, [download and install Node.js](https://nodejs.org/en/download/).
Node.js 0.10 or higher is required.

Install all the necessary modules by:

```bash
$ npm install
```

Open ``.env`` file in editor and update the values based on your system.

```
SOCKET_IO_PORT=8080
NODE_ENV=development
MONGODB_URI=mongodb://127.0.0.1:27017/
MONGODB_DB_NAME=smarthomesystem_dev
RABBITMQ_URI=amqp://localhost
PORT=3000
LOGGER_LEVEL=debug
LOGGER_ENABLED=true
ANALYZER_CLOCK=20000
```

## Test

  This is not a finished project so let's just run the test environment. First we have to import test data fixtures. 
  To do so, run the command below:
  
  ```bash
  $ npm test
  ```
  When it loads all the data exit.
  
  There are 5 more commands we can run.
  
  Start the app.
  ```bash
  $ npm start
  ```
  This will start the server so you can go hit the browser ``localhost:3000/rooms`` or  ``localhost:3000/sensors``
  You will see a list but no data. It is time to mock sensors! Just run in another terminal:
  
  ```bash
  $ npm mock-sensors
  ```

  After it start sending data to MQ run this command to see them on the graph:
  
  ```bash
  $ npm websocket
  ```
  Now you can go visit ``Room A`` to see how sensors are doing.
  But none of these data are saved to database. To do so, run the command below:
  
  ```bash
  $ npm record-sensors
  ```
  
  Now all the data from sensors are being recorded and you can see real time changes on the browser. If you wait long 
  enough you will realize that room temperature is not stable. To stop this we need to control devices that heat up the room.
  We can turn them ON or OFF. To this automatically we can run the following command and this will check the temperature of the sensors
  and it will send events to turn ON and OFF the Heater/AC.
    
  ```bash
  $ npm analyze
  ```
  Now if you look -long enough- at the real-time room sensor temperature graph, you can see that the room temperature slowly getting stabilized.
  Ideal temperature and other limits are saved per room record.
   
   
  
