/**
 * Sensor Mocking
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict'

/**
 * Get sensor mocker
 */
module.exports = require('./mocker')
