/**
 * Sensor Mocker
 * @author Ali Atasever <aliatasever@gmail.com>
 */

'use strict'

/**
 * Get dependencies
 */
const async = require('async')
const sensorDb = require('../../../lib/repository/sensor')
const switchDb = require('../../../lib/repository/switch')
const rabbitmqService = require('../../../lib/service/rabbitmq')

/**
 * Set temperatures and temperature ranges
 */
const temperatures = {}
const roomTemperatureRanges = {}

/**
 * Get list of sensors to mock
 */
sensorDb.getSensors(null, function (promise) {
  promise.then(function(sensors) {
    async.parallel([
      startSwitchListening(),
      startSensorMocking(sensors)
    ], function(err, results) {
      throw new Error(err)
    });
  }).catch(function (exception) {
    console.log(exception.message)
  })
})

/**
 * Starts sensor mocking
 * @todo: get time values from config
 * @param sensors
 */
let startSensorMocking = function (sensors) {
  rabbitmqService.getConnection()
  async.forEachOf(sensors, function (sensor, key, callback) {
    if (sensor.type === 'temperature') {
      const q = sensor._id.toString()
      setInterval(function () {
        let temperature = getTemperature(sensor)
        console.log('Sensor #' + q + ' is at ' + temperature);
        rabbitmqService.publish(q, 'sensors', {
          temperature: temperature,
          date: new Date(),
        }, {expiration: '10000'})
      }, 5000)
    }
  }, function (err) {
    if (err) console.error(err.message)
  })
}

/**
 * Get random temperature for sensor
 * @param sensor
 * @returns {*}
 */
function getTemperature (sensor) {
  let sensorId = sensor._id.toString()

  if(!(sensor.roomId in roomTemperatureRanges)){
    roomTemperatureRanges[sensor.roomId] = [-1, 0, 1]
  }

  if (sensorId in temperatures) {
    temperatures[sensorId] =  temperatures[sensorId] + roomTemperatureRanges[sensor.roomId][Math.floor(Math.random() * roomTemperatureRanges[sensor.roomId].length)]

    return temperatures[sensorId]
  }

  return temperatures[sensorId] = 20
}

/**
 * Start listening switch events for testing switches
 */
let startSwitchListening = function () {
  switchDb.getSwitches().then(function(switches) {
    let switchIds = switches.map(function(a) {return a._id});
    rabbitmqService.getConnection().then(function () {
      let queue = 'sensor_mocker_switch_events';
      rabbitmqService.consume(queue, 'switches', switchIds.join('.'), false, function (message) {
        let switchObj = switches.find(function (a) { return a._id.toString() === message.routingKey})
        let event = JSON.parse(message.content)
        if(event.apply === 1 && switchObj.type === 'two-state-heater'){
          console.log(' [*] ' + switchObj.name + ' turning "ON" ...')
          roomTemperatureRanges[switchObj.roomId] = [0, 1]
        }else if(event.apply === -1 && switchObj.type === 'two-state-heater'){
          console.log(' [*] ' + switchObj.name + ' turning "OFF" ...')
          roomTemperatureRanges[switchObj.roomId] = [-1, 0]
        }
      })
    })
  })
}
